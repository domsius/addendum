<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class PostFactory extends Factory
{
    public function definition()
    {
        return [
            'title' => $this->faker->word,
            'post_text' => $this->faker->text(500),
            'category_id' => $this->faker->randomDigitNotNull(),
        ];
    }
}

